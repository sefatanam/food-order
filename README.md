# Food Ordering 

### Features
- [ ] Able to create bulk order
- [ ] Customer list

### How to run

 - clone this repository
 - install dependencies via `npm install`
 - after installing dependencies run `npm start`
 - locate this app from your browser via `http://localhost:42000`


 See Live : [Demo Link](https://food-order-neon.vercel.app/order/list)
