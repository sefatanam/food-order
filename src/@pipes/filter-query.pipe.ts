import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterQuery'
})
export class FilterQueryPipe implements PipeTransform {

  transform(value: string) {
    return value.split('?')[0].replace('-', ' ');
  }

}
