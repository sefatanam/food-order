import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SorterPipe} from "./sorter.pipe";
import {SentenceCasePipe} from "./sentence-case.pipe";
import { FilterQueryPipe } from './filter-query.pipe';


@NgModule({
  declarations: [
    SorterPipe,
    SentenceCasePipe,
    FilterQueryPipe,
  ],
  exports: [
    SentenceCasePipe,
    SorterPipe,
    FilterQueryPipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule {
}
