import {Component, EventEmitter, Input, Output} from '@angular/core';

export interface SelectData {
  name: string;
  value: string;
}

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent {

  @Input() label: string;
  @Input() placeholder: string;
  @Input() control: any;
  @Input() dataList: SelectData[] = [];
  @Output() onChange: EventEmitter<number> = new EventEmitter<number>();
  @Input() bindLabel: string;
  @Input() bindValue: string;

  get showErrors() {
    return this.control?.touched && this.control?.errors;
  };

  _onChange = ($event: any) => this.onChange.emit($event.detail.value);

}
