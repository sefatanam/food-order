import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavComponent} from './nav/nav.component';
import {TableComponent} from './table/table.component';
import {InputComponent} from './input/input.component';
import {ReactiveFormsModule} from "@angular/forms";
import {SelectComponent} from './select/select.component';
import {RadioComponent} from './radio/radio.component';
import {NgSelectModule} from "@ng-select/ng-select";
import {PipesModule} from "../@pipes/pipes.module";


@NgModule({
  declarations: [
    NavComponent,
    TableComponent,
    InputComponent,
    SelectComponent,
    RadioComponent,
  ],
  exports: [
    NavComponent,
    TableComponent,
    InputComponent,
    SelectComponent,
    RadioComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgSelectModule,
    PipesModule
  ]
})
export class ComponentsModule {
}
