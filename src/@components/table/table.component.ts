import {Component, Input, TemplateRef} from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {
  @Input() tableHeader: TemplateRef<any>
  @Input() tableBody: TemplateRef<any>
  @Input() titles: string[] = [];
  @Input() dataList: any[] = [];
  @Input() isSortable = true;

  sortName = '';
  templist = [];

  sortByCol(name: string) {

    if (!this.isSortable) return;
    // name = name.split(' ').join('');

    if (this.templist.length > 0) {
      if (this.templist.indexOf(name) !== -1) {
        this.templist.pop();
      } else {
        this.templist.push(name);
      }
    } else {
      this.templist.push(name);
    }

    this.sortName = this.templist.join();
  }
}
