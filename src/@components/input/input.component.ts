import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent {
  // @ts-ignore
  @Input() control: any;
  @Input() label!: string;
  @Input() placeholder: string = '';
  @Input() rightIcon!: string;
  @Input() inputType: 'text' | 'number' = 'text';
  @Input() isTitle: boolean = false;
  @Input() isErrorShown: boolean = true;
}
