import {Control} from "@pages/dynamic-form/create-control/create-control.component";

export const DUMMY_CONTROLS: Control[] = [
  {
    id: 1,
    name: 'Phone Number',
    type: 'number',
    formId: 1,
    placeholder: 'Enter your phone number'
  }, {
    id: 2,
    name: 'Full Name',
    type: 'text',
    formId: 1,
    placeholder: 'Enter your full name'
  }, {
    id: 3,
    name: 'Gender',
    type: 'radio',
    formId: 1,
    data: [
      {
        id: 1,
        name: 'Male',
        value: 'male'
      },
      {
        id: 2,
        name: 'Female',
        value: 'female'
      }
    ]
  },
  {
    id: 4,
    name: 'City',
    type: 'select',
    formId: 1,
    data: [
      {
        id: 1,
        name: 'NY',
        value: 'NY'
      },
      {
        id: 2,
        name: 'CA',
        value: 'CA'
      }
    ]
  },
  {
    id: 5,
    name: 'Hobby',
    formId: 1,
    type: 'checkbox',
    data: [
      {
        id: 1,
        name: 'Reading',
        value: 'Reading'
      },
      {
        id: 2,
        name: 'Traveling',
        value: 'Traveling'
      },
      {
        id: 2,
        name: 'Swimming',
        value: 'Swimming'
      }
    ]
  }
]
