import { Order } from "@pages/order/models/order";

export const ORDERS: Order[] = [
    {
        name: "Sefat",
        contactNo: "234567",
        items: [
            {
                name: "Burger x 1",
                price: 12,
                quantity: 123,
                id: 1,
                userId: 1
            },
            {
                name: "Chips x 1",
                price: 12,
                quantity: 23,
                id: 2,
                userId: 1
            }
        ],
        id: 1
    },
    {
        name: "Anam",
        contactNo: "123456",
        items: [
            {
                name: "Burger x 2",
                price: 12,
                quantity: 123,
                id: 3,
                userId: 2
            },
            {
                name: "Chips x 2",
                price: 12,
                quantity: 23,
                id: 4,
                userId: 2
            }
        ],
        id: 2
    },
    {
        name: "Farook",
        contactNo: "23423",
        items: [
            {
                name: "Burger x 3",
                price: 12,
                quantity: 123,
                id: 5,
                userId: 3
            },
            {
                name: "Chips x3",
                price: 12,
                quantity: 23,
                id: 6,
                userId: 3
            }
        ],
        id: 3
    },
    {
        name: "Humayan",
        contactNo: "343423",
        items: [
            {
                name: "Burger x 4",
                price: 12,
                quantity: 123,
                id: 7,
                userId: 4
            },
            {
                name: "Chips x 4",
                price: 12,
                quantity: 23,
                id: 8,
                userId: 4
            }
        ],
        id: 4
    }
];
