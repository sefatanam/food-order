import {Injectable} from '@angular/core';
import {BehaviorSubject, of} from "rxjs";
import {Control} from "@pages/dynamic-form/create-control/create-control.component";
import {DUMMY_CONTROLS} from "@services/data/DUMMY_CONTROLS";

@Injectable({
  providedIn: 'root'
})
export class DynamicFormService {

  private controlList$ = new BehaviorSubject<Control[]>([])
  private formList$ = new BehaviorSubject<{ id: number, name: string }[]>([

  ]);

  getAll() {
    return this.controlList$.asObservable();
  }

  getAllForms() {
    return this.formList$.asObservable();
  }



  createControl(control: Control) {
    if (control.id <= 0) {
      control.id = this.controlList$.value.length + 1;
      this.controlList$.next([...this.controlList$.value, control])
    } else {
      const findIndex = this.controlList$.value.findIndex(obj => obj.id == control.id);
      this.controlList$.value[findIndex].name = control.name
      this.controlList$.next(this.controlList$.value)
    }

  }

  createForm(args: { id: number, name: string }) {

    if (args.id <= 0) {
      args.id = this.formList$.value.length + 1;
      this.formList$.next([...this.formList$.value, args])
    } else {
      const findIndex = this.formList$.value.findIndex(obj => obj.id == args.id);
      this.formList$.value[findIndex].name = args.name
      this.formList$.next(this.formList$.value)
    }


  }

  getFormById(id: number) {
    return of(this.formList$.value.find((order) => order.id === id));
  }

  getControlsByFormId(id: number) {
    return of(this.controlList$.value.filter((control) => control.formId === id));
  }
}
