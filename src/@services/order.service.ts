import {Injectable} from '@angular/core';
import {BehaviorSubject, of} from "rxjs";
import {Order} from "@pages/order/models/order";
import {ORDERS} from './data/DUMMY_ORDERS';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private orders$ = new BehaviorSubject<Array<Order>>(ORDERS);

  getAll() {
    return this.orders$.asObservable();
  }

  create(order: Order): void {

    order.id = this.orders$.value.length + 1;
    this.orders$.next([...this.orders$.value, order])
  }

  getById(id: number) {
    return of(this.orders$.value.find((order) => order.id === id));
  }

  delete(id: number) {
    const freshList = this.orders$.value.filter((d) => d.id !== id);
    this.orders$.next(freshList);
  }
}
