import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {OrderService} from "@services/order.service";
import {map, Observable, switchMap} from "rxjs";
import {Order} from "@pages/order/models/order";

@Component({
  selector: 'app-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.scss']
})
export class OrderViewComponent implements OnInit {


  order: Observable<Order>;
  tableColumns = ['Name', 'Quantity', 'Price', 'Total'];

  constructor(private _activatedRoute: ActivatedRoute,
              private _orderService: OrderService) {
  }

  ngOnInit(): void {
    this.order = this._activatedRoute.paramMap.pipe(
      switchMap((paramMap: any) => {
        const userId = paramMap.params.userId;
        return this._orderService.getById(+userId).pipe(
          map((data) => data)
        )
      })
    )
  }


}
