import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OrderViewComponent} from "@pages/order/order-view/order-view.component";

const routes: Routes = [
  {
    path: ':userId',
    component: OrderViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderViewRoutingModule {
}
