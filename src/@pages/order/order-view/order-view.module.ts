import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderViewRoutingModule } from './order-view-routing.module';
import { OrderViewComponent } from './order-view.component';
import {ComponentsModule} from "@components/components.module";


@NgModule({
  declarations: [
    OrderViewComponent
  ],
  imports: [
    CommonModule,
    OrderViewRoutingModule,
    ComponentsModule
  ]
})
export class OrderViewModule { }
