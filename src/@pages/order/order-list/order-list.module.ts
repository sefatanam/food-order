import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OrderListRoutingModule} from './order-list-routing.module';
import {OrderListComponent} from './order-list.component';
import {ComponentsModule} from "@components/components.module";


@NgModule({
  declarations: [
    OrderListComponent
  ],
  imports: [
    CommonModule,
    OrderListRoutingModule,
    ComponentsModule
  ],
  providers: []
})
export class OrderListModule {}
