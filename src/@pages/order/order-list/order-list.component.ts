import {Component, OnInit} from '@angular/core';
import {Order} from "@pages/order/models/order";
import {OrderService} from "@services/order.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-customer-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  titles = ['name', 'contactNo'];
  dataList: Observable<Array<Order>>

  constructor(private orderService: OrderService) {
  }

  ngOnInit(): void {
    this.dataList = this.orderService.getAll();
  }

}
