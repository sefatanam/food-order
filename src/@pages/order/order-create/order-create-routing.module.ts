import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderCreateComponent } from "@pages/order/order-create/order-create.component";
import { OrderRouteDeactiveGuard } from '../order-route-deactive.guard';

const routes: Routes = [
  {
    path: '',
    component: OrderCreateComponent,
    canDeactivate: [OrderRouteDeactiveGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderCreateRoutingModule { }
