import { Injectable } from "@angular/core";
import { AbstractControl, ValidationErrors, Validator } from "@angular/forms";

@Injectable({
    providedIn: 'root'
})
export class LocalPhoneNumber implements Validator {
    validate(control: AbstractControl): ValidationErrors {
        const value: string = control.value;
        const isValid = value.match(/(^(\+8801|8801|01|008801))[1|3-9]{1}(\d){8}$/);
        return isValid === null ? { localPhoneNumber: true } : null;
    }

}