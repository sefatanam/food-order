import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Item, Order } from "@pages/order/models/order";
import { OrderService } from "@services/order.service";
import { ActivatedRoute, CanDeactivate, Router, UrlTree } from "@angular/router";
import { Observable, of, switchMap } from 'rxjs';
import { LocalPhoneNumber } from './validators/local-phone-number.validator';
import { CanComponentDeactivate, IDeactivateComponent } from '../order-route-deactive.guard';

export enum Mode {
  CREATE = 'create',
  EDIT = 'edit'
}

@Component({
  selector: 'app-order-create',
  templateUrl: './order-create.component.html',
  styleUrls: ['./order-create.component.scss']
})
export class OrderCreateComponent implements OnInit, IDeactivateComponent {

  orderForm !: FormGroup;

  tableColumns = ['Name', 'Quantity', 'Price', 'Total'];

  constructor(private _formBuilder: FormBuilder,
    private _orderService: OrderService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute) {
  }


  //Check if there any unsaved data etc. If yes then as for confirmation
  canExit(): boolean {
    if (confirm("Do you wish to Please confirm")) {
      return true
    } else {
      return false
    }
  }

  ngOnInit(): void {

    this._activatedRoute.queryParams
      .pipe(
        switchMap((qp) => {
          if (qp['mode'] === Mode.EDIT) {
            return this._activatedRoute.queryParams.pipe(
              switchMap((params: any) => {
                return this._orderService.getById(+params['userId'])
              })
            )
          } else {
            return of(new Order());
          }
        })
      )
      .subscribe((order) => {
        this.buildForm(order)
      });

  }

  buildForm(order: Order = new Order()) {
    this.orderForm = this._formBuilder.group({
      name: [order.name, [Validators.required]],
      contactNo: [order.contactNo, [Validators.required, Validators.minLength(11), Validators.maxLength(13), new LocalPhoneNumber()]],
      items: new FormArray([]),
      id: [order.id]
    });

    if (order?.items?.length === 0) {
      this.addNewOrderItem();
    } else {
      order?.items.forEach((item) => this.updateExistingItem(item))

    }

  }


  updateExistingItem(item: Item) {
    (this.orderForm.get('items') as FormArray).push(this._formBuilder.group({
      name: [item.name, [Validators.required]],
      price: [item.price, [Validators.required]],
      quantity: [item.quantity, [Validators.required]],
      id: [item.id]
    }));
  }

  addNewOrderItem() {

    const items = this.orderForm.get('items') as FormArray;

    try {
      if (items.controls[items.length - 1].status === 'INVALID') {
        this.orderForm.markAllAsTouched();
      } else {
        items.push(this._formBuilder.group({
          name: ['', [Validators.required]],
          price: ['', [Validators.required]],
          quantity: ['', [Validators.required]],
          id: [0]
        }));
      }
    } catch (e) {
      items.push(this._formBuilder.group({
        name: ['', [Validators.required]],
        price: ['', [Validators.required]],
        quantity: ['', [Validators.required]],
        id: ['']
      }));
    }


  }

  removeOrderItem(index: number) {

    (this.orderForm.get('items') as FormArray).removeAt(index)
  }

  async submit() {
    if (this.orderForm.invalid) {
      this.orderForm.markAllAsTouched();
      return;
    }

    console.log(this.orderForm.value);
    this._orderService.create(this.orderForm.value);
    this.buildForm();
    alert("Order Created.")
    await this._router.navigate(['/order/list'])
  }


}
