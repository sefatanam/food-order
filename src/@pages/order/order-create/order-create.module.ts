import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderCreateRoutingModule } from './order-create-routing.module';
import { OrderCreateComponent } from './order-create.component';
import { ComponentsModule } from "@components/components.module";
import { ReactiveFormsModule } from "@angular/forms";
import { OrderRouteDeactiveGuard } from '../order-route-deactive.guard';


@NgModule({
  declarations: [
    OrderCreateComponent
  ],
  imports: [
    CommonModule,
    OrderCreateRoutingModule,
    ComponentsModule,
    ReactiveFormsModule
  ],
  providers: [OrderRouteDeactiveGuard]
})
export class OrderCreateModule { }
