export class Order {
  id?: number = 0;
  name: string = '';
  contactNo: string = '';
  items?: Item[] = []
}

export class Item {
  id?: number = 0;
  name: string = '';
  price: number = 0;
  quantity: number = 0;
  userId: number = 0;
}
