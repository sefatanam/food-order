import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { OrderCreateComponent } from './order-create/order-create.component';
import { OrderComponent } from './order.component';

export interface CanComponentDeactivate {
  canDeactivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree;
}

export interface IDeactivateComponent {
  canExit(): Observable<boolean> | Promise<boolean> | boolean;
}
@Injectable({
  providedIn: 'root'
})
export class OrderRouteDeactiveGuard implements CanDeactivate<OrderCreateComponent> {
  component: Object;
  route: ActivatedRouteSnapshot;

  constructor() {
  }

  canDeactivate(component: IDeactivateComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
    nextState: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return component.canExit ? component.canExit() : true;
  }
}
