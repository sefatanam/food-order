import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderRouteDeactiveGuard } from './order-route-deactive.guard';
import { OrderComponent } from "./order.component";

const routes: Routes = [
  {
    path: '',
    component: OrderComponent,
    children: [
      {
        path: "list",
        loadChildren: () => import('./order-list/order-list.module').then(m => m.OrderListModule),

      }, {
        path: "view",
        loadChildren: () => import('./order-view/order-view.module').then(m => m.OrderViewModule)

      }, {
        path: "create",
        loadChildren: () => import('./order-create/order-create.module').then(m => m.OrderCreateModule),
      }, {
        path: "",
        redirectTo: "list",
        pathMatch: "full"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule {
}
