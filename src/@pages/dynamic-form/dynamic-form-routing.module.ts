import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DynamicFormComponent} from "@pages/dynamic-form/dynamic-form.component";

const routes: Routes = [
  {
    path: '',
    component: DynamicFormComponent,
    children: [
      {
        path: 'create-control',
        loadChildren: () => import('./create-control/create-control.module').then(m => m.CreateControlModule)
      }, {
        path: 'create-form',
        loadChildren: () => import('./create-form/create-form.module').then(m => m.CreateFormModule)
      }, {
        path: 'form-list',
        loadChildren: () => import('./form-list/form-list.module').then(m => m.FormListModule)
      }, {
        path: 'control-list',
        loadChildren: () => import('./control-list/control-list.module').then(m => m.ControlListModule)
      }, {
        path: 'view',
        loadChildren: () => import('./form-view/form-view.module').then(m => m.FormViewModule)
      },
      {
        path: '',
        redirectTo: 'form-list',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DynamicFormRoutingModule {
}
