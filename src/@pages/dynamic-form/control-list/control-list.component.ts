import {Component, OnInit} from '@angular/core';
import {DynamicFormService} from "@services/dynamic-form.service";
import {Observable, of, switchMap} from "rxjs";
import {Control} from "@pages/dynamic-form/create-control/create-control.component";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-control-list',
  templateUrl: './control-list.component.html',
  styleUrls: ['./control-list.component.scss']
})
export class ControlListComponent implements OnInit {
  dataList: Observable<Control[]>
  titles = ['name', 'type', 'placeholder'];
  formNo: number = null;

  constructor(private _dynamicFormService: DynamicFormService,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {

    this.dataList = this._activatedRoute.queryParams
      .pipe(
        switchMap((qp) => {
          if (qp['form-no']) {
            this.formNo = qp['form-no']
            return this._dynamicFormService.getControlsByFormId(+qp['form-no'])
          } else {
            return of([]);
          }
        })
      )
  }

}
