import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ControlListComponent} from "@pages/dynamic-form/control-list/control-list.component";

const routes: Routes = [
  {
    path: '',
    component: ControlListComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ControlListRoutingModule {
}
