import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ControlListRoutingModule } from './control-list-routing.module';
import { ControlListComponent } from './control-list.component';
import {ComponentsModule} from "@components/components.module";


@NgModule({
  declarations: [
    ControlListComponent
  ],
    imports: [
        CommonModule,
        ControlListRoutingModule,
        ComponentsModule
    ]
})
export class ControlListModule { }
