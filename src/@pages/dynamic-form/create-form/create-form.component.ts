import {Component, OnInit} from '@angular/core';
import {DynamicFormService} from "@services/dynamic-form.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {of, switchMap} from "rxjs";
import {Mode} from "@pages/order/order-create/order-create.component";

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.scss']
})
export class CreateFormComponent implements OnInit {

  form!: FormGroup;

  constructor(private _dynamicFormService: DynamicFormService,
              private _formBuilder: FormBuilder,
              private _router: Router,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this._activatedRoute.queryParams
      .pipe(
        switchMap((qp) => {
          if (qp['mode'] === Mode.EDIT) {
            return this._activatedRoute.queryParams.pipe(
              switchMap((params: any) => {
                return this._dynamicFormService.getFormById(+params['form'])
              })
            )
          } else {
            return of({id: 0, name: ''});
          }
        })
      )
      .subscribe((form) => {
        this.buildForm(form)
      });
  }

  buildForm(args?: { name: string, id: number }) {
    this.form = this._formBuilder.group({
      id: [args?.id ?? 0],
      name: [args?.name ?? '', [Validators.required]]
    })
  }

  async submit() {

    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    this._dynamicFormService.createForm(this.form.value)
    await this._router.navigate(['/dynamic-form/form-list'])
  }
}
