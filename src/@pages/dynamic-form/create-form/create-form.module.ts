import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateFormRoutingModule } from './create-form-routing.module';
import { CreateFormComponent } from './create-form.component';
import {ReactiveFormsModule} from "@angular/forms";
import {ComponentsModule} from "@components/components.module";


@NgModule({
  declarations: [
    CreateFormComponent
  ],
  imports: [
    CommonModule,
    CreateFormRoutingModule,
    ReactiveFormsModule,
    ComponentsModule
  ]
})
export class CreateFormModule { }
