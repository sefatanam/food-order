import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateFormComponent} from "@pages/dynamic-form/create-form/create-form.component";

const routes: Routes = [
  {
    path: '',
    component: CreateFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateFormRoutingModule {
}
