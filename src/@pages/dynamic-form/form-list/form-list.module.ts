import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormListRoutingModule } from './form-list-routing.module';
import { FormListComponent } from './form-list.component';
import {ComponentsModule} from "@components/components.module";


@NgModule({
  declarations: [
    FormListComponent
  ],
    imports: [
        CommonModule,
        FormListRoutingModule,
        ComponentsModule
    ]
})
export class FormListModule { }
