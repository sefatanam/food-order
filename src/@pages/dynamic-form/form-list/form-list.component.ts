import {Component, OnInit} from '@angular/core';
import {DynamicFormService} from "@services/dynamic-form.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-form-list',
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.scss']
})
export class FormListComponent implements OnInit {
  dataList: Observable<{ id: number, name: string }[]>;
  titles = ['name'];

  constructor(private _dynamicFormService: DynamicFormService) {
  }

  ngOnInit(): void {
    this.dataList = this._dynamicFormService.getAllForms();
  }

}
