import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateControlRoutingModule } from './create-control-routing.module';
import { CreateControlComponent } from './create-control.component';
import {ReactiveFormsModule} from "@angular/forms";
import {ComponentsModule} from "@components/components.module";
import {NgSelectModule} from "@ng-select/ng-select";


@NgModule({
  declarations: [
    CreateControlComponent
  ],
  imports: [
    CommonModule,
    CreateControlRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    NgSelectModule
  ]
})
export class CreateControlModule { }
