import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ControlDataValidator} from "@pages/dynamic-form/create-control/validators/control-data.validator";
import {SelectData} from "@components/select/select.component";
import {map} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {DynamicFormService} from "@services/dynamic-form.service";


export type ControlType = 'text' | 'number' | 'select' | 'date' | 'radio' | 'checkbox';

export class ControlData {
  id = 0;
  name = '';
  value = '';
}

export class Control {
  id = 0;
  name = '';
  formId: number = 0;
  placeholder? = '';
  type: ControlType = 'text';
  data?: ControlData[] = []
}


@Component({
  selector: 'app-create-form',
  templateUrl: './create-control.component.html',
  styleUrls: ['./create-control.component.scss']
})
export class CreateControlComponent implements OnInit {

  controlForm !: FormGroup;
  typeList: SelectData[] = [
    {
      name: 'Text',
      value: 'text'
    },
    {
      name: 'Number',
      value: 'number'
    },
    {
      name: 'Checkbox',
      value: 'checkbox'
    },
    {
      name: 'Select',
      value: 'select'
    }
  ];
  private _formNo = 0;

  constructor(private _formBuilder: FormBuilder,
              private _router: Router,
              private _dynamicFormService: DynamicFormService,
              private _controlDataValidator: ControlDataValidator,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.buildForm();
    this._activatedRoute.queryParams
      .pipe(
        map((qp) => {
          return qp['form-no'];
        })
      ).subscribe((formId: string) => {
      this._formNo = parseInt(formId);
      this.controlForm.patchValue({formId: parseInt(formId)})
    })
  }

  buildForm(control: Control = new Control()) {
    debugger;
    this.controlForm = this._formBuilder.group({
      id: [control?.id],
      name: [control.name],
      placeholder: [control.name],
      formId: [control.formId],
      type: [control.type],
      data: new FormArray([])
    }, {
      validators: [
        this._controlDataValidator.validate
      ]
    })
  }


  updateExistingControl(item: ControlData) {
    (this.controlForm.get('data') as FormArray).push(this._formBuilder.group({
      name: [item?.name, [Validators.required]],
      value: [item?.value, [Validators.required]],
      id: [item?.id]
    }));
  }

  addNewControl() {

    const data = this.controlForm.get('data') as FormArray;

    try {
      if (data.controls[data.length - 1].status === 'INVALID') {
        this.controlForm.markAllAsTouched();
      } else {
        data.push(this._formBuilder.group({
          name: ['', [Validators.required]],
          value: ['', [Validators.required]],
          id: [0]
        }));
      }
    } catch (e) {
      data.push(this._formBuilder.group({
        name: ['', [Validators.required]],
        value: ['', [Validators.required]],
        id: [0]
      }));
    }


  }

  removeControl(index: number) {
    (this.controlForm.get('items') as FormArray).removeAt(index)
  }


  async submit() {
    if (this.controlForm.invalid) {
      this.controlForm.markAllAsTouched();
      return;
    }

    this._dynamicFormService.createControl(this.controlForm.value);
    await this._router.navigate(['/dynamic-form/control-list'], {queryParams: {'form-no': this._formNo}})
  }
}
