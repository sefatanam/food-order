import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateControlComponent} from "@pages/dynamic-form/create-control/create-control.component";

const routes: Routes = [
  {
    path: '',
    component: CreateControlComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateControlRoutingModule {
}
