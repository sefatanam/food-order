import {FormGroup, ValidationErrors, Validator} from "@angular/forms";
import {Injectable} from "@angular/core";
import {ControlType} from "@pages/dynamic-form/create-control/create-control.component";


@Injectable({
  providedIn: 'root'
})
export class ControlDataValidator implements Validator {
  validate(control: FormGroup): ValidationErrors | null {
    const {type, data} = control.value;

    const isAssignableData = ((type as ControlType) !== 'text' || (type as ControlType) !== 'number');

    if (isAssignableData && data.length < 0) {
      return {controlDataRequired: true};
    }
    return null;
  }
}
