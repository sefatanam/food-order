import {Component, OnInit} from '@angular/core';
import {DynamicFormService} from "@services/dynamic-form.service";
import {Control} from "@pages/dynamic-form/create-control/create-control.component";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {of, switchMap} from "rxjs";

@Component({
  selector: 'app-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.scss']
})
export class FormViewComponent implements OnInit {

  controlList: Control[];
  form: FormGroup = new FormGroup({});
  // TODO : load with data in DOM
  formName: string;

  constructor(private _dynamicFormService: DynamicFormService,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {


    this.getAll();
    this.makeForm();
    console.log(this.form.controls)
  }

  // TODO : get all controls list
  getAll() {

    this._activatedRoute.queryParams
      .pipe(
        switchMap((qp) => {
          if (qp['form-no']) {
            return this._dynamicFormService.getControlsByFormId(+qp['form-no'])
          } else {
            return of([]);
          }
        }),
      ).subscribe((data) => this.controlList = data)

    this._activatedRoute.queryParams
      .pipe(
        switchMap((qp) => {
          if (qp['form-no']) {
            return this._dynamicFormService.getFormById(+qp['form-no'])
          } else {
            return of([]);
          }
        }),
      ).subscribe((data: { id: number, name: string }) => this.formName = data?.name)

    // this._dynamicFormService.getControlsByFormId().subscribe((data) => this.controlList = data);
  }

  // TODO : took out all control name property
  makeForm() {
    this.controlList.forEach(control => {
      const controlName = control.name.split(' ').join('');
      if (control.type === 'checkbox') {
        this.form.addControl(controlName, new FormArray([], [Validators.required]));
        this.patchValues(controlName, control.data)
      } else {
        this.form.addControl(controlName, new FormControl('', [Validators.required])
        )
      }
    })
  }

  // TODO : making control for each control

  // TODO : add control to a formGroup

  // TODO : generate form in HTML

  patchValues(formArrayName: string, data: any[]) {
    const formArray = this.form.get(formArrayName) as FormArray;
    data.forEach((option) => {
      formArray.push(
        new FormGroup({
          name: new FormControl(option.name),
          checked: new FormControl(false),
        })
      );
    });
  }

  submit() {
    console.log(this.form.value)
  }
}
