import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormViewRoutingModule } from './form-view-routing.module';
import { FormViewComponent } from './form-view.component';
import {ComponentsModule} from "@components/components.module";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    FormViewComponent
  ],
  imports: [
    CommonModule,
    FormViewRoutingModule,
    ComponentsModule,
    ReactiveFormsModule
  ]
})
export class FormViewModule { }
