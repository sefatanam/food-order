import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CustomerListRoutingModule} from './customer-list-routing.module';
import {CustomerListComponent} from './customer-list.component';
import {ComponentsModule} from "@components/components.module";


@NgModule({
  declarations: [
    CustomerListComponent
  ],
  imports: [
    CommonModule,
    CustomerListRoutingModule,
    ComponentsModule
  ]
})
export class CustomerListModule {}
