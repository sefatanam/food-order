import {Component} from '@angular/core';
import {Order} from "@pages/order/models/order";

@Component({
  selector: 'app-customer-order-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent {

  titles = ['Name', 'Contact No'];
  dataList: Order[] = [
    {
      name: 'Sefat',
      contactNo: '01854191869'
    }
  ]


}
