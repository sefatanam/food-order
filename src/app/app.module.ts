import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsModule } from "@components/components.module";
import { PipesModule } from "../@pipes/pipes.module";
import { OrderRouteDeactiveGuard } from '@pages/order/order-route-deactive.guard';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  providers: [OrderRouteDeactiveGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
