import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'order',
    loadChildren: () => import('../@pages/order/order.module').then(m => m.OrderModule)
  }, {
    path: 'customer',
    loadChildren: () => import('../@pages/customer/customer.module').then(m => m.CustomerModule)
  }, {
    path: 'dynamic-form',
    loadChildren: () => import('../@pages/dynamic-form/dynamic-form.module').then(m => m.DynamicFormModule)
  }, {
    path: '',
    redirectTo: 'order',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
