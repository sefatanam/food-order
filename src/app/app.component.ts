import {Component} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  urls: string[];
  open: boolean;

  constructor(private _router: Router) {
  }


  ngOnInit(): void {

    this._router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const url = event.url;
        this.urls = url.split('/');
      }
    });
  }

  getFinalUrl(index: number): string {
    return this.urls.slice(0, index + 1).join('/');
  }
}
